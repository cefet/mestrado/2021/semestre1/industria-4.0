# Indústria 4.0

A indústria 4.0 é um tema atual e envolve diversos temas de modelagem matemática computacional.

Nesta disciplina serão vistos diversos conceitos e ferramentas utilizadas na indústria 4.0, repassadas por docentes que trabalham em empresas que sobrevivem da indústria 4.0.

Serão vistas ferramentas de programação em alto nível, modelagem computacional, otimização, inteligência computacional, processamento em nuvem e segurança. Tudo em uma linguagem direta e aplicada, com ensaios de programação durante a aula.
